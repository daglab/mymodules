<?php

namespace Ucheniki\Mymodule;

use Illuminate\Support\ServiceProvider;

class MymoduleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/config/mymodule.php','module');
        $this->publishes([
            __DIR__.'/config'=>base_path('config'),
        ]);
        $modules = config("module.modules");
        $dir = app_path().'/Modules';
        
   
    if($modules) { 
        while (list(,$module) = each($modules)){ 
       //Подключаем роуты для модуля 
       
                if(file_exists($dir.'/'.$module.'/Routes/routes.php')) { 
                   $this->loadRoutesFrom($dir.'/'.$module.'/Routes/routes.php');
                }
                
                 //Загружаем View
                //view('Test::admin')
                if(is_dir($dir.'/'.$module.'/Views')) {
                    $this->loadViewsFrom($dir.'/'.$module.'/Views', $module);
                }
                
                //Подгружаем миграции
                if(is_dir($dir.'/'.$module.'/Migration')) {
                    $this->loadMigrationsFrom($dir.'/'.$module.'/Migration');
                }
                //Подгружаем переводы
                //trans('Test::messages.welcome')
                if(is_dir($dir.'/'.$module.'/Lang')) {
                    $this->loadTranslationsFrom($dir.'/'.$module.'/Lang', $module);
                }
            }
        }
    }
}
